__iconsData: {
    
        'acrobat': {
            width: '30px',
            height: '30px'
        },
    
        'add': {
            width: '30px',
            height: '30px'
        },
    
        'ajust': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-big': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-round-bright': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-round': {
            width: '30px',
            height: '29px'
        },
    
        'arrow-small': {
            width: '30px',
            height: '30px'
        },
    
        'attach': {
            width: '30px',
            height: '30px'
        },
    
        'attention-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'backup': {
            width: '30px',
            height: '30px'
        },
    
        'calculator': {
            width: '30px',
            height: '30px'
        },
    
        'calendar': {
            width: '30px',
            height: '30px'
        },
    
        'camera': {
            width: '68px',
            height: '50px'
        },
    
        'cancel': {
            width: '30px',
            height: '30px'
        },
    
        'chat': {
            width: '30px',
            height: '30px'
        },
    
        'close-big': {
            width: '30px',
            height: '30px'
        },
    
        'close-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'close': {
            width: '30px',
            height: '30px'
        },
    
        'close_1': {
            width: '30px',
            height: '30px'
        },
    
        'code': {
            width: '30px',
            height: '30px'
        },
    
        'creative-commons': {
            width: '30px',
            height: '30px'
        },
    
        'docs': {
            width: '33px',
            height: '42px'
        },
    
        'done-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'done': {
            width: '30px',
            height: '30px'
        },
    
        'dot': {
            width: '30px',
            height: '30px'
        },
    
        'download': {
            width: '30px',
            height: '30px'
        },
    
        'edit': {
            width: '30px',
            height: '30px'
        },
    
        'expand': {
            width: '30px',
            height: '30px'
        },
    
        'external-link': {
            width: '30px',
            height: '30px'
        },
    
        'facebook': {
            width: '30px',
            height: '30px'
        },
    
        'help': {
            width: '30px',
            height: '30px'
        },
    
        'hide': {
            width: '30px',
            height: '30px'
        },
    
        'hide_1': {
            width: '30px',
            height: '30px'
        },
    
        'ic-nav-1': {
            width: '72px',
            height: '63px'
        },
    
        'ic-nav-2': {
            width: '63px',
            height: '63px'
        },
    
        'ic-nav-3': {
            width: '46px',
            height: '67px'
        },
    
        'ic-nav-4': {
            width: '30px',
            height: '63px'
        },
    
        'ic-nav-5': {
            width: '80px',
            height: '64px'
        },
    
        'ic-nav-6': {
            width: '47px',
            height: '63px'
        },
    
        'ic-nav-7': {
            width: '54px',
            height: '63px'
        },
    
        'keyboard-arrow': {
            width: '30px',
            height: '30px'
        },
    
        'language': {
            width: '30px',
            height: '30px'
        },
    
        'link': {
            width: '30px',
            height: '30px'
        },
    
        'location': {
            width: '30px',
            height: '30px'
        },
    
        'map': {
            width: '19px',
            height: '19px'
        },
    
        'menu': {
            width: '30px',
            height: '30px'
        },
    
        'minimize': {
            width: '30px',
            height: '30px'
        },
    
        'money': {
            width: '30px',
            height: '30px'
        },
    
        'phone': {
            width: '30px',
            height: '30px'
        },
    
        'piece': {
            width: '30px',
            height: '30px'
        },
    
        'play-new': {
            width: '48px',
            height: '48px'
        },
    
        'print': {
            width: '30px',
            height: '30px'
        },
    
        'reload': {
            width: '30px',
            height: '30px'
        },
    
        'reminder': {
            width: '30px',
            height: '30px'
        },
    
        'rss': {
            width: '30px',
            height: '30px'
        },
    
        'search': {
            width: '30px',
            height: '30px'
        },
    
        'settings-big': {
            width: '30px',
            height: '30px'
        },
    
        'settings-small': {
            width: '30px',
            height: '30px'
        },
    
        'shedule': {
            width: '30px',
            height: '30px'
        },
    
        'signal': {
            width: '30px',
            height: '30px'
        },
    
        'sound-off': {
            width: '30px',
            height: '30px'
        },
    
        'sound-on': {
            width: '30px',
            height: '30px'
        },
    
        'sound': {
            width: '30px',
            height: '30px'
        },
    
        'time': {
            width: '30px',
            height: '30px'
        },
    
        'timer': {
            width: '30px',
            height: '30px'
        },
    
        'user-big': {
            width: '30px',
            height: '30px'
        },
    
        'user': {
            width: '30px',
            height: '30px'
        },
    
        'visible': {
            width: '30px',
            height: '30px'
        },
    
        'wait': {
            width: '30px',
            height: '30px'
        },
    
        'warning': {
            width: '30px',
            height: '30px'
        },
    
        'youtube': {
            width: '30px',
            height: '30px'
        },
    
}