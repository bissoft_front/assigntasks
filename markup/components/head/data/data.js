head: {
    defaults: {
        title: 'Індекс',
        useSocialMetaTags: false
    },
    home: {
        title: 'Головна',
        useSocialMetaTags: false
    },
    about: {
        title: 'Про проект',
        useSocialMetaTags: false
    },
    project: {
        title: 'Проекти',
        useSocialMetaTags: false
    },
    consultations: {
        title: 'Консультації',
        useSocialMetaTags: false
    },
    investors: {
        title: 'Інвестори',
        useSocialMetaTags: false
    },
    instructions: {
        title: 'Інструкції',
        useSocialMetaTags: false
    },
    law: {
        title: 'Нормативно-правова база',
        useSocialMetaTags: false
    },
    persona: {
        title: 'Депутат',
        useSocialMetaTags: false
    },
    news: {
        title: 'Новини',
        useSocialMetaTags: false
    },
    post: {
        title: 'Новина',
        useSocialMetaTags: false
    }
}
